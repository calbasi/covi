*Initialize RCS System

ci -d file ; Insert file into RCS (-d preserves file date)
co file ; Pulls read-only copy from RCS system)
covi -i file ; Performs both steps, asks for file description

*Edit files

covi -i file ; Creates RCS file/file,v (initializes RCS system)
covi file
covi -r 2.0 file ; Specifies that the new version will be 2.0 
                    ; (it has to be larger than the current one)

Edit file normally with vi, and when finished write a change log.


*Other useful RCS commands

rlog file ; List changes made to the file
rcsdiff -r1.0 file ; Displays diff of changes made since version 1.0
co -p1.0 file : Displays version 1.0 of the file on screen

Translated with www.DeepL.com/Translator (free version)
